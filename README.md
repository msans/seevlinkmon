# seevlinkmon

Monitor & manage Schneider Electric EVLink data

## Required Packages

* tomli
* pandas

## Usage

```
usage: seevlinkmon.py [options] [DATABASEFOLDER]

seevlinkmon.py monitor & manage Schneider Electric EVLink data

positional arguments:
  DATABASEFOLDER        path to the folder containing the database files

options:
  -h, --help            show this help message and exit
  -n, --no-backup       disable backup
  -s, --strict-fusion   strict fusion: no duplicate allowed
  -i INPUT_DATA_FILE, --input-data-file INPUT_DATA_FILE
                        path to the input csv file containing data to be inserted in the database
```

## Init a database

A database contains the recharge data for a particular SE EVlink station (`CS_ID`). Note that a particular station might
be used to recharge several EV: A charge data record csv file is potentially a mixture of the data from different EV. 

Let's create the folder `<station_name>` that will contain the database for a specific par Evlink Station, 
e.g. the station at home called `evse`: 

```bash
mkdir evse
```

In the above created folder, create a `station_id.toml` file, containing the following information about the station:

```toml
commercial_ref = "EVBXXXXXXXX"  # 11 alphanum characters code
factory_code = "XX"             # 2 alphanum characters code
production_date = "20001"       # 5 num characters code
production_revision = "01"      # 2 num characters code
batch_number = "012"            # 3 num characters code
unique_identifier = "123"       # 3 num characters code
production_id = "XXXXXXX"       # 7 alphanum characters code
```

create an empty database file `station_complete_database.pkl`:

```python
import pandas as pd
df = pd.DataFrame(columns = ['CDR_ID','CS_ID','Socketoutlet_ID','Transaction_ID','UID','Type of charge', 'Start_Datetime','End_Datetime','Energy_kWh','Socket_Type','Duration','Comment'])
df.to_pickle('station_complete_database.pkl')
```

## Insert a Recharge Report in the Database

E.g. insert a weekly log 

```bash
python seevlinkmon.py  -i EVBXXXXXXXXXX2000101012123XXXXXXX_20221219-00_00_WLog.csv path/to/evse
```