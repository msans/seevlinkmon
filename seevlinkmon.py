#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: msans
"""
import sys
import argparse
import pathlib
import shutil
import tomli
import pandas as pd
from datetime import datetime as dt


class SEEVLinkMon:
    """Schneider Electric EVLink monitoring class"""
    def __init__(self, database_folder: str):

        self.data_record_header = [
            'CDR_ID','CS_ID','Socketoutlet_ID','Transaction_ID','UID','Type of charge',
            'Start_Datetime','End_Datetime','Energy_kWh','Socket_Type','Duration','Comment']
        self.database_folder = database_folder
        self.station_id_filestem = 'station_id'
        self.station_id_fileext = '.toml'
        self.station_cdb_filestem = 'station_complete_database'
        self.station_cdb_fileext = '.pkl'
        self.station_id_file = pathlib.Path(database_folder, self.station_id_filestem + self.station_id_fileext)
        self.station_cdb_file = pathlib.Path(database_folder, self.station_cdb_filestem + self.station_cdb_fileext)

        self.err = False
        self.err_msg = ''

        self.station_id = dict()
        self.cs_id = ''
        self.commercial_ref = ''
        self.factory_code = ''
        self.production_date = ''
        self.production_revision = ''
        self.batch_number = ''
        self.unique_identifier = ''
        self.production_id = ''
        self.cdb = None
        self._get_user_data()



    def _get_user_data(self) -> bool:
        """Get user data (site location, etc.) from a toml file"""

        try:
            with open(self.station_id_file, 'rb') as f:
                self.station_id = tomli.load(f)
        except (IOError, tomli.TOMLDecodeError) as err:
            self.err_msg = f"ERROR: could not read config file {self.station_id_file}: " + str(err)
            self.err = True
            return True

        try:
            self.commercial_ref = self.station_id['commercial_ref']
            self.factory_code = self.station_id['factory_code']
            self.production_date = self.station_id['production_date']
            self.production_revision = self.station_id['production_revision']
            self.batch_number = self.station_id['batch_number']
            self.unique_identifier = self.station_id['unique_identifier']
            self.production_id = self.station_id['production_id']

        except (KeyError) as err:
            self.err_msg = f"ERROR: reading config file {self.station_id_file}: " + str(err)
            self.err = True
            return True

        self.cs_id = self.commercial_ref + self.factory_code + self.production_date + self.production_revision + \
                     self.batch_number + self.unique_identifier + self.production_id

        try:
            self.cdb = pd.read_pickle(self.station_cdb_file)
        except Exception as err:
           self.err_msg = f"ERROR: could not read database file {self.station_cdb_file}: " + str(err)
           self.err = True
           return True

        return False

    def fusion_data(self, datafile: str, nobackup: bool, strict_fusion: bool):

        ts = dt.now().isoformat()
        if not(nobackup):
            try:
                shutil.copyfile(self.station_cdb_file,
                                str(pathlib.Path(self.database_folder,
                                                 ts+'_'+self.station_cdb_filestem+self.station_cdb_fileext)))
            except Exception as err:
                self.err_msg = f"ERROR: could not backup database file {self.station_cdb_file}: " + str(err)
                self.err = True
                return True
        try:
            data_record = pd.read_csv(datafile, sep=';')
        except Exception as err:
            self.err_msg = f"ERROR: could not read data record file {datafile}: " + str(err)
            self.err = True
            return True

        if any(self.cdb.columns != data_record.columns):
            self.err = True
            self.err_msg = f"ERROR: unexpected column names, input file: {datafile}"
            return True

        if strict_fusion:
            if data_record['CDR_ID'].isin(self.cdb['CDR_ID']).any():
                self.err = True
                self.err_msg = f"ERROR: at least 1 CDR_ID entry already in the database, input file: {datafile}"
                return True

        cdb_fusion = pd.concat([self.cdb, data_record]).sort_values('CDR_ID', axis=0, ascending=True)

        for idx in range(1, len(cdb_fusion)):
            if cdb_fusion.iloc[idx]['CDR_ID'] == cdb_fusion.iloc[idx-1]['CDR_ID']:
                if not cdb_fusion.iloc[idx,:].equals(cdb_fusion.iloc[idx-1, :]):
                    self.err = True
                    self.err_msg = f"ERROR: found non-duplicate row with same CDR_ID {cdb_fusion.iloc[idx, 0]}, during fusion of input file: {datafile}"
                    return True

        cdb_fusion.drop_duplicates(keep='first', inplace=True, ignore_index=True)

        for idx in range(1,len(cdb_fusion)):
            curr_start = dt.fromisoformat(cdb_fusion.iloc[idx]['Start_Datetime'])
            prev_end = dt.fromisoformat(cdb_fusion.iloc[idx-1]['End_Datetime'])
            if prev_end > curr_start:
                self.err = True
                self.err_msg = f"ERROR: found recharge period overlap during fusion of input file: {datafile}"
                return True

        self.cdb = cdb_fusion


        try:
            self.cdb.to_pickle(self.station_cdb_file)
        except Exception as err:
           self.err_msg = f"ERROR: could not write database file {self.station_cdb_file}: " + str(err)
           self.err = True
           return True

        return  False

    def is_error(self):
        return self.err

    def get_error(self):
        return self.err_msg

def seevlinkmon() -> int:
    """Monitor & manage Schneider Electric EVLink data"""

    # argument parsing
    script_path = pathlib.Path(__file__).absolute()
    usage = "%(prog)s [options] [DATABASEFOLDER]"
    description = script_path.name + " monitor & manage Schneider Electric EVLink data"
    parser = argparse.ArgumentParser(usage=usage, description=description)

    parser.add_argument("-n", "--no-backup", dest="nobackup", action='store_true',
                        help="disable backup")
    parser.add_argument("-s", "--strict-fusion", dest="strict_fusion", action='store_true',
                        help="strict fusion: no duplicate allowed")
    parser.add_argument("-i", "--input-data-file", dest="input_data_file", action='store', type=str,
                        help="path to the input csv file containing data to be inserted in the database")
    parser.add_argument("database_folder",
                        action="store", nargs="?", metavar="DATABASEFOLDER",
                        help="path to the folder containing the database files")

    args = parser.parse_args()

    # init
    mon = SEEVLinkMon(args.database_folder)
    if mon.is_error():
        print(mon.get_error())
        return 1

    mon.fusion_data(args.input_data_file, args.nobackup, args.strict_fusion)
    if mon.is_error():
        print(mon.get_error())
        return 1


    return 0

if __name__ == '__main__':
    sys.exit(seevlinkmon())
